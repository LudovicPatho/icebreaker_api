import random
from typing import Optional
import flask
from flask import jsonify

def create_app():

    app = flask.Flask(__name__)

    with open("/app/data.txt", "r") as f:
        data = f.read().splitlines() 
        print(data)

    @app.route('/api', methods=['GET'])
    def api_root():
        print(data)
        return {"text": random.choice(data)}
    
    @app.route('/api/ping', methods=['GET'])
    def ping():
        return jsonify(ping='pong')

    @app.route('/api/<integer>')
    def multiple_sentences(integer):
        if integer.isdigit():
            return {"data": [{"text": random.choice(data)} for i in range(int(integer))]}
        else:
            return {"Error" : "Must be an integer"}


    return app

if __name__ == '__main__':
    app = create_app()
    app.debug=True
    app.run(host='0.0.0.0', port=5002)
