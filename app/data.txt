Who do you think is a great example of a successful person?
As a child, what did you want to become when you grew up?
What is your greatest achievement so far?
What was your childhood’s nickname and why?
What is the most difficult part of your job?
Tell me a funny story that happened to you.
Who has been your best mentor so far? In what area?
What hobby or sport activity are you good at?
What would you rather do when you are not working?
Share a memorable event of your life with me.
What would you do if you won the lottery? How would you spend the money for yourself other than giving some to others?
What are you most proud of in your life?
What is the most challenging task or project you have ever been involved in?
What do you most appreciate in life?
What is great about living in the country you live in?
What do you most appreciate for living in this era?
Which five cities do you want to visit, that you have not visited yet, before you die?
Why are you here?
What is the most influential book you have ever read?
In your opinion, what is the secret to mastery of any given skill?
What do you know about the “law of attraction”? What do you think of it?
If you were diagnosed with a terminal illness and were told you only have a year to live, what would be the most exciting thing you would do?
If you had a chance to meet three celebrities, who would you pick?
What does family mean to you?
What is your most favourite board game?
What is your most favourite sport?
What is your most favourite computer game?
If you were told you can have any car, what model would you choose?
What is your favourite food?
What is your favourite movie?
Would you rather have a dog or a cat? Why?
There has been a scientific breakthrough eliminating the need to sleep. What would you do with all the extra time?
What do you live for?
What is your favourite documentary?
What is your typical day like?
Suppose, you can take a pill and learn a skill instantly; what skill would you choose?
What five books would you recommend to people to read if they didn’t have a chance to read anything else?
What recent purchases of a £100 or less have you made that had a significant positive impact on your life?
What is the best question you heard recently? How would you answer it?
What is the most interesting conversation you had recently?
What is the greatest threat to humanity’s survival?
What is the most exciting technological advance in the coming decade?
What is your morning ritual?
What is your most favourite tool?
What is your most favourite app?
What is your most favourite device?
What do you do in the first 60 minutes of your day?
What’s one thing that you do that people think is crazy, but you do it anyway?
What painting has evoked great emotions out of you?
What is your favourite work of art?
What is your most favourite fiction? And why?
When was the last time you changed your mind on an important issue and how did it happen?
What is the book (or books) you have most often gifted to other people?
What makes you different?
What are you world-class at?
What was the happiest moment of your life?
Who is your most favourite dead person in history?
What was your most favourite toy as a child?
What is the strangest thing you have ever done?
Which country seems most exotic to you?
What is the greatest lesson you have learn about life so far?
What three animals would you choose that describe your traits the best?
If you could use a time travel machine, which period of history would you choose to visit?
What is the one thing for which you are most likely to be remembered after your death?
What would you like to be known for?
What is your most favourite question to ask? How do you answer it yourself?
What do people never ask you that you wish they did?